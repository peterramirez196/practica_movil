package com.example.peter.practica;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Actividad4 extends AppCompatActivity  implements View.OnClickListener , Frguno.OnFragmentInteractionListener,frgdos.OnFragmentInteractionListener{

    Button button2, button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad4);
        button= (Button) findViewById(R.id.frg1);
        button2= (Button) findViewById(R.id.frg2);
        button.setOnClickListener(this);
        button2.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.frg1:
                Frguno fragmentoUno =new Frguno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.frg2:

                frgdos fragmentoDos =new frgdos();
                FragmentTransaction transactiondos = getSupportFragmentManager().beginTransaction();
                transactiondos.replace(R.id.contenedor,fragmentoDos);
                transactiondos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
