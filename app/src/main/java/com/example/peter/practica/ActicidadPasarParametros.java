package com.example.peter.practica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActicidadPasarParametros extends AppCompatActivity {


    EditText editText;
    Button button2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acticidad_pasar_parametros);


        editText= (EditText) findViewById(R.id.editText);
        button2= (Button) findViewById(R.id.button2);


        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ActicidadPasarParametros.this,
                        ActividadRecibirParametros.class);
                Bundle bundle =new Bundle();

                bundle.putString("dato",editText.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


    }




}
