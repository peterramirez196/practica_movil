package com.example.peter.practica;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.main,menu);

        return  true;
    }


    public boolean onOptionsItemSelected (MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionlogi:

                Dialog dialoglogin=new  Dialog(MainActivity.this);
                dialoglogin.setContentView(R.layout.dlg_login);
                Button botonAutentificar= (Button) dialoglogin.findViewById(R.id.autentificar);
                final EditText cajausuario=(EditText) dialoglogin.findViewById(R.id.usuario);
                final  EditText cajacable= (EditText) dialoglogin.findViewById(R.id.clave) ;
                dialoglogin.show();
                botonAutentificar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this,cajausuario.getText().toString() + " "+cajacable.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });
                break;
            case R.id.opcionRegistro:
                intent = new Intent(MainActivity.this,

                        Actividad1.class);
                startActivity(intent);
                break;
            case R.id.opcionParametro:
                intent = new Intent(MainActivity.this,

                        ActicidadPasarParametros.class);
                startActivity(intent);
                break;
            case R.id.opcionMain:
                intent = new Intent(MainActivity.this,

                        Actividad4.class);
                startActivity(intent);
                break;
        }
        return true;
    }


    public void vistadocentes(View view) {

        Intent intent = new Intent(MainActivity.this,
                ActicidadPasarParametros.class);
        startActivity(intent);

    }


}
