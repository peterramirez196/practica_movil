package com.example.peter.practica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class ActividadRecibirParametros extends AppCompatActivity {
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_recibir_parametros);

        textView=(TextView) findViewById(R.id.textView) ;
        Bundle bundle = this.getIntent().getExtras();
        textView.setText(bundle.getString("dato"));
    }
}
