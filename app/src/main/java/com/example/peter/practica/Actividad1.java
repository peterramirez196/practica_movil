package com.example.peter.practica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Actividad1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad1);
    }

    public void vistalogi(View view) {

        Intent intent = new Intent(Actividad1.this,
                login.class);
        startActivity(intent);

    }
    public void vistaregistro(View view) {

        Intent intent = new Intent(Actividad1.this,
                registro.class);
        startActivity(intent);

    }
    public void vistaguardar(View view) {

        Intent intent = new Intent(Actividad1.this,
                guardar.class);
        startActivity(intent);

    }
}
